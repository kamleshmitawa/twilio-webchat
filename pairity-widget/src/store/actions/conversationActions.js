import CONVERSATION_TYPE from '../types'
import store from '../store';

const { dispatch } = store

export const chatScreenOpen = (data) => {
    return dispatch({
        type: CONVERSATION_TYPE.CHAT_SCREEN_OPEN,
        payload : data
    })
}
export const chatScreenClose = (data ) => {
    return dispatch({
        type: CONVERSATION_TYPE.CHAT_SCREEN_CLOSE,
        payload : data 
    })
}

export const conversationList = data => {
    return dispatch({
        type: CONVERSATION_TYPE.CONVERSATION_LIST,
        payload : data
    })
}
