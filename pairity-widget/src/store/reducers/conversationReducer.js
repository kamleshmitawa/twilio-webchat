import CONVERSATION_TYPE from '../types'

const defaultState = {
    conversationList : [],
    userData : []
}

export const conversationReducer = (state = defaultState,action) => {
    switch(action.type){
        case CONVERSATION_TYPE.CONVERSATION_LIST:
            return { ...state, conversationList : [...state.conversationList, action.payload.name] }

        case CONVERSATION_TYPE.CHAT_SCREEN_OPEN:
            let data = state.userData;
            let indexVal = data.findIndex(x => x.name === action.payload.name);
            if(indexVal === -1){
                if(data.length >= 3){
                    state.userData.unshift(action.payload)
                    return { ...state, userData : state.userData }           
                }
                else 
                return { ...state, userData : [...state.userData, action.payload]  }           
            }
            else {
                return { ...state, userData: state.userData}
            }
        
        case CONVERSATION_TYPE.CHAT_SCREEN_CLOSE:
            let closeData = state.userData
            let index = closeData.findIndex(x => x.name === action.payload.name);
            state.userData.splice(index, 1)
            return { ...state, userData : state.userData }

        default :
            return state
    }
}