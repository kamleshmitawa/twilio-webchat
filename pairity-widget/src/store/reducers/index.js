import { combineReducers } from 'redux'
import { conversationReducer } from "./conversationReducer";


export const reducers = combineReducers({
  conversation: conversationReducer
});
