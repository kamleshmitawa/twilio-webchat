import uuidv1 from "uuid/v1";
import { baseUrl } from "../config";
import { getRequest } from "../api/api";

export const fetchChatToken = async () => {
  try {
    let res = await getRequest(`/web/token/${uuidv1()}`)
    console.log(res, "res.data");
    return res.data.jwt;
  } catch (err) {
    console.log(err, "generate_token error");
  }
};



