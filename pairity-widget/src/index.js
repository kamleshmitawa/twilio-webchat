let poly = require("preact-cli/lib/lib/webpack/polyfills");

import { h } from "preact";
import habitat from "preact-habitat";

import Widget from './App';


const Main = props => (
    <Widget Props={props} />
);

let _habitat = habitat(Main);

_habitat.render({
  selector: '[data-widget-host="habitat"]',
  clean: true
});







// _habitat.render({
//   selector: ".preview",
//   clean: true
// });
