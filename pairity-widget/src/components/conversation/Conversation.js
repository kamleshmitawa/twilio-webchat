import React, { Component } from "react";
import { connect } from "preact-redux";
import { fetchChatToken } from "../../utils/utils";
import uuidv1 from 'uuid/v1'
import ChatScreen from "./ChatScreen";
import { postRequest } from "../../api/api";
import axios from "axios";
import { baseUrl } from "../../config";


const Twilio = window.Twilio;

class Conversation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isChatScreen: false,
      inputMessage: "",
      messageList: []
    };
  }


  onChatCreate = () => {
    fetchChatToken().then(token => {
      Twilio.Chat.Client.create(token).then(client => {

        console.log(token, 'token', client)

        if (this.chatClient) return;
        this.chatClient = client;

        client.on("channelJoined", channel => {
                    console.log(channel.members, "channelchannel");

                        channel.on("messageAdded", message => {
                          const { messageList } = this.state;
                          this.setState({ messageList: [...messageList, message.state.body] });
                        });

                        channel.on("memberJoined", member => {
                          console.log("member Joined", member.identity);
                        });

                        channel.on("memberLeft", member => {
                          console.log("member Left", member.identity);
                        });

                        channel.on("memberInfoAdded", member => {
                          console.log("member info updated", member.identity);
                        });

                        channel.on("typingStarted", member => {
                          console.log("typing started", member.identity);
                        });

                        channel.on("tytpingEnded", member => {
                          console.log("member Left", member.identity);
                        });

                        channel.invite("elmo").then(() => {
                          console.log("Your friend has been invited!");
                        });
        });

        client.on("channelAdded", channel => {
          this.setState({ isChatScreen: true });
          console.log(channel, 'newchannel')
          console.log("Channel added: " + channel.friendlyName);
        });

        client.on("channelRemoved", channel => {
          console.log("channel Removed", channel.friendlyName);
        });

        client.on("channelUpdated", channel => {
          console.log("channel Updated=============", channel.friendlyName);
        });

        client.on("channelInvited", channel => {
          console.log("Invited to channel " + channel.friendlyName);
          // channel.join();
        });

        client.on("tokenExpired", this.HandleRefereshToken);

        this.getChatRoom()
          .then(channel => {
            console.log(channel, "channel")
          })
          .catch(err => console.log(err, "create channel error"));
      });
    })

  };

  getChatRoom = () => {
    return this.chatClient
      .createChannel({
        friendlyName: uuidv1(),
        isPrivate: false,
        uniqueName: ''
      })
      .then(channel => {
        console.log(channel, "----channel");
        this.channel = channel;
        if (channel.state.status !== "joined") {
          channel.join()
        }
        return channel;
      })
      .catch(err => console.log(err, "---=====createChannel error"));
  }

  HandleRefereshToken = () => {
    return fetchChatToken().then(token => {
      this.chatClient.updateToken(token);
    });
  };

  handleChange = e => {
    console.log(e.target.value, 'eee')
    this.setState({ inputMessage: e.target.value });
  };


  sendMessageToAgent = () => {
    const { inputMessage } = this.state;
    console.log(this.channel.sid, "channelsid", inputMessage, 'inputMessage');

    let data = {
      channelId: this.channel.sid, 
      message: inputMessage,
      isSentByAgent:false
    }
    return postRequest('/web/sendMessage',data )
      .then(res => {
        console.log(res, "sendMessage res");
        this.setState({ inputMessage: "" });
      })
      .catch(err => console.log(err, " sendMessage err"));
  };


  onEnterSendMessage = e => {
    console.log(e.key, 'e.key')
    if (e.key === "Enter") {
      this.sendMessageToAgent();
    }
  };

  render() {
    const { loading, messageList } = this.state

    console.log( messageList, 'messageList')

    return (
      <div className="conversation-block">
        {this.state.isChatScreen ?
        // <ChatScreen onEnterSendMessage={this.onEnterSendMessage}  handleChange={this.handleChange} sendMessageToAgent={this.sendMessageToAgent} inputMessage={this.state.inputMessage} />
           <div className="conversation-list-block">
          <div className="messages-block"> msg
          {/* {
            messageList && messageList.map(msg => (

            ))
          } */}
            </div>
          <div>
            <textarea
              onKeyDown={this.onEnterSendMessage}
              name="body"
              className="message-area"
              placeholder="type your message....."
              onChange={this.handleChange}
              value={this.state.inputMessage}
            />
          </div>

          <div className="message-button-block">
            <button className="message-button" onClick={this.sendMessageToAgent}>Send Message</button>
          </div>
        </div> 
        : (
            <div className="conversation-header" onClick={this.onChatCreate}>
              Create
          </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps)(Conversation);
