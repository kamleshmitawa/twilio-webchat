import React, { Component } from 'react'


class ChatScreen extends Component {
    constructor(props) {
        super(props);

    }
    handleChange = (e) => {
        let msg = e.target.value;
        this.props.handleChange(msg);
    }
    changeVal = (e) => {
        console.log(e.target.value, 'changeVal');

    }
    render() {
        console.log(this.props, 'props ChatScreen')
        return (
            <div className="conversation-list-block">
                <div className="messages-block">messages  </div>
                <div>
                    <input type="text" onChange={this.changeVal} />
                    <textarea
                        onKeyDown={(e)=> this.props.onEnterSendMessage(e)}
                        name="body"
                        className="message-area"
                        placeholder="type your message....."
                        onChange={e => this.handleChange(e)}
                        value={this.props.inputMessage}
                    />
                </div>

                <div className="message-button-block">
                    <button className="message-button" onClick={this.props.sendMessageToAgent}>Send Message</button>
                </div>
            </div>
        )
    }
}

export default ChatScreen