import axios from 'axios'
import { baseUrl } from '../config';

const axiosInstance = axios.create({
    baseURL: baseUrl
})


export const getRequest = url => {
    return axiosInstance({
        method: 'GET',
        url
    })
}


export const postRequest = (url, data) => {
    console.log(data, 'dataaaa')
    return axiosInstance({
        method: 'POST',
        url,
        data
    })
}