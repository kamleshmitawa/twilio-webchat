import React, { Component } from "react";
import { Provider } from "preact-redux";
import store from "./store/store";
import { firebaseConfig } from './firebaseConfig'; 
import firebase from 'firebase'
import Conversation from "./components/conversation/Conversation";

class App extends Component {

  componentDidMount(){
    if (firebase) {
      console.log('config', firebaseConfig)
      firebase.initializeApp(firebaseConfig);

    }
  }

  render() {
    console.log('widget')
    return (
      <Provider store={store}>
        <Conversation />
      </Provider>
    );
  }
}

export default App;
