import React, { Component } from "react";
import { connect } from "react-redux";
import Conversation from "./conversation/Conversation";
import ChatScreen from "./conversation/ChatScreen";
import MultipleChatScreen from "./conversation/MultipleChatScreen";

class MainComponent extends Component {
  render() {
    const { userData = [] } = this.props.conversation;
    return (
      <div>
        <Conversation />
        {userData && userData.length > 0 ? (
          <div className="chat-screen-block-all">
            <ul className="multiple-chat-block">
              {userData.map((element, index) =>
                index >= 3 ? (
                  <MultipleChatScreen dataId={element.channel_id} key={index} />
                ) : null
              )}
            </ul>
            <ul className="chat-screen-block">
              {userData.map((element, index) =>
                index <= 2 ? (
                  <ChatScreen dataId={element.channel_id} key={index} />
                ) : null
              )}
            </ul>
          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    conversation: state.conversation
  };
};

export default connect(
  mapStateToProps,
  {}
)(MainComponent);
