import React, { Component } from "react";
import { connect } from "react-redux";
import conversation from "../../store/actions";
import ChatMessage from "./ChatMessage";
import { fetchChatToken } from "../../utils/utils";
import { postRequest } from "../../api/api";

let Twilio = window.Twilio;

class ChatScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isCollapse: false,
      inputMessage: ""
    };
  }

  componentDidMount = async () => {
    this.initialChatToken();
  };

  componentWillUnmount() {
    console.log("unmount");
  }

  initialChatToken = async () => {
    fetchChatToken().then(token => {

      Twilio.Chat.Client.create(token).then(client => {

        if (this.chatClient) return;
        this.chatClient = client;

        client.on("channelJoined", channel => {
          console.log(channel.members, "channelchannel");

          channel.on("messageAdded", message => {
            console.log("messageAddedmessageAdded", message);
            conversation.newMessage(message);
          });

          channel.on("memberJoined", member => {
            console.log("member Joined", member.identity);
          });

          channel.on("memberLeft", member => {
            console.log("member Left", member.identity);
          });

          channel.on("memberInfoAdded", member => {
            console.log("member info updated", member.identity);
          });

          channel.on("typingStarted", member => {
            console.log("typing started", member.identity);
          });

          channel.on("typingEnded", member => {
            console.log("member Left", member.identity);
          });

          channel.invite("elmo").then(() => {
            console.log("Your friend has been invited!");
          });
        });

        client.on("channelAdded", channel => {
          console.log("Channel added:=========" + channel.friendlyName);
        });

        client.on("channelRemoved", channel => {
          console.log("channel Removed", channel.friendlyName);
        });

        client.on("channelUpdated", channel => {
          console.log("channel Updated", channel.friendlyName);
        });

        client.on("channelInvited", channel => {
          console.log("Invited to channel " + channel.friendlyName);
          channel.join();
        });

        client.on("tokenExpired", this.HandleRefereshToken);

        this.getChatRoom()
          .then(channel => {
            console.log(channel, "channel");
            this.setState({ loading: false });
          })
          .catch(err => console.log(err, "create channel error"));
      });
    });
  };

  getChatRoom = () => {
    const { conversationData } = this.props;
    console.log(conversationData.channel_id, "data.channel_iddata.channel_id");
    return this.chatClient
      .getChannelBySid(conversationData.channel_id)
      .then(channel => {
        this.channel = channel;
        if (channel.state.status !== "joined") {
          channel.join();
        }
        return channel;
      })
      .catch(err => console.log(err, "---=====createChannel error"));
  };

  HandleRefereshToken = () => {
    return fetchChatToken().then(token => {
      this.chatClient.updateToken(token);
    });
  };

  closeChatScreen = (e, data) => {
    conversation.chatScreenClose(data);
    e.stopPropagation();
  };

  sendMessageToCustomer = () => {
    const { inputMessage } = this.state;

    let data = {
      channelId: this.channel.sid,
      message: inputMessage,
      isSentByAgent: true
    };

    return postRequest("/web/agent-send-message", data)
      .then(res => {
        console.log(res, "sendMessage res");
        this.setState({ inputMessage: "" });
      })
      .catch(err => console.log(err, " sendMessage err"));
  };

  onEnterSendMessage = e => {
    if (e.key === "Enter") {
      this.sendMessageToCustomer();
    }
  };

  render() {
    const { isCollapse, inputMessage, loading } = this.state;
    const { conversationData } = this.props;
    const { messages = [] } = conversationData;
   
    return (
      <li
        className={
          isCollapse
            ? "chat-screen-block-item chat-screen-collapse-block"
            : "chat-screen-block-item"
        }
      >
        <div
          className="conversation-header"
          onClick={() => this.setState({ isCollapse: !this.state.isCollapse })}
        >
          <img
            src="assests/images/avatar.png"
            className="chat-user-profile-image"
            alt="profileimg"
          />
          <span className="chat-user-name">
            {conversationData && conversationData.unique_name
              ? "Anonymous1"
              : "Anonymous"}
          </span>
          <i
            onClick={e => this.closeChatScreen(e, conversationData)}
            className={"fa fa-times-circle-o chat-close-icon"}
            aria-hidden="true"
          ></i>
        </div>
        <div className="chat-block">
          {/* {loading ? "loading" : <ChatMessage messages={messages} />} */}
          <ChatMessage messages={messages} />
        </div>
        <div className="chat-message-input-block">
          <input
            onKeyDown={this.onEnterSendMessage}
            value={inputMessage}
            onChange={e => this.setState({ inputMessage: e.target.value })}
            type="text"
            placeholder="Message Input ..."
            name="input"
            className="chat-message-input"
          />

          <button onClick={this.sendMessageToCustomer}>Send</button>
        </div>
      </li>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  console.log(state, "ChatScreenstate", ownProps);
  return {
    conversationList1: state.conversation.conversationList,

    conversationData: state.conversation.conversationList.find(
      val => val.channel_id === ownProps.dataId
    )
  };
};

export default connect(mapStateToProps)(ChatScreen);
