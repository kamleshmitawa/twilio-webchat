import React, { Component } from "react";

class ChatMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { messages } = this.props;

    return (
      <div className="display-messages">
        {messages && messages.length > 0
          ? messages.map((item, index) =>
              !JSON.parse(item.attributes).isSentByAgent ? (
                <div key={index} className="display-messages-item-customer">
                  <span className="message-item-customer">{item.body}</span>
                </div>
              ) : (
                <div key={index} className="display-messages-item-agent">
                  <span className="message-item-agent">{item.body}</span>
                </div>
              )
            )
          : null}
      </div>
    );
  }
}

export default ChatMessage;
