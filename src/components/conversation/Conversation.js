import React, { Component } from "react";
import { connect } from "react-redux";
import conversation from "../../store/actions";
import { getRequest } from "../../api/api";

class Conversation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isBlockClose: false
    };
  }

  componentDidMount() {
    getRequest("/web/getChannel")
      .then(res => {
        console.log(res.data.data, 'getChannelres')
        this.setState({
          loading: false
        });
        conversation.conversationList(res.data.data);

      })
      .catch(err => console.log(err, "getChannel error"));
  }

  handleChatScreen = data => {
    conversation.chatScreenOpen(data);
  };


  render() {
    const { isBlockClose, loading } = this.state;
    const { conversationList = [] } = this.props;

    return (
      <div
        className={
          isBlockClose ? "conversation-block close-block" : "conversation-block"
        }
      >
        <div
          className="conversation-header"
          onClick={() => this.setState({ isBlockClose: false })}
        >
          Conversation
        </div>
        <div onClick={() => this.setState({ isBlockClose: true })}>
          <i
            className={
              isBlockClose
                ? "fa fa-times-circle-o close-icon close-block"
                : "fa fa-times-circle-o close-icon"
            }
            aria-hidden="true"
          ></i>
        </div>
        <div className="conversation-list-block">
          {conversationList && conversationList.length > 0 ? (
            conversationList.map((item, index) => (
              <div
                className="conversation-list-item"
                key={index}
                onClick={() => this.handleChatScreen(item)}
              >
                <img
                  src="assests/images/avatar.png"
                  className="profile-image"
                  alt="profileimg"
                />
                <div className="user-name">
                  {index}
                  {/* {item && item.unique_name ? "Anonymous1" : "Anonymous"} */}
                </div>
                <div className="conversation-list-message">
                  {item && item.messages && item.messages.length > 0
                    ? item.messages[item.messages.length - 1].body
                    : null}
                </div>
              </div>
            ))
          ) :
            conversationList && conversationList.length === 0 ?
              <div className="conversation-list-item">No Conversations</div>
              :
              (
                <div className="conversation-list-item">...loading</div>
              )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    conversationList: state.conversation.conversationList
  };
};

export default connect(mapStateToProps)(Conversation);
