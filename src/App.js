import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "./store/store";
import MainComponent from "./components/MainComponent";
import { firebaseConfig } from "./firebaseConfig";
import firebase from "firebase";

class App extends Component {
  componentDidMount() {
    if (firebase) {
      console.log("config", firebaseConfig);
      firebase.initializeApp(firebaseConfig);
    }
  }

  render() {
    return (
      <Provider store={store}>
        <MainComponent />
      </Provider>
    );
  }
}

export default App;
