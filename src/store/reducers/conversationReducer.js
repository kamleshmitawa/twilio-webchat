import CONVERSATION_TYPE from "../types";

const defaultState = {
  conversationList: [],
  userData: []
};

export const conversationReducer = (state = defaultState, action) => {
  switch (action.type) {
    case CONVERSATION_TYPE.CONVERSATION_LIST:
      return {
        ...state,
        conversationList: action.payload
      };

    case CONVERSATION_TYPE.CHAT_SCREEN_OPEN:
      let data = state.userData;
      let indexVal = data.findIndex(
        x => x.channel_id === action.payload.channel_id
      );
      if (indexVal === -1) {
        if (data.length >= 3) {
          state.userData.unshift(action.payload);
          return { ...state, userData: state.userData };
        } else
          return { ...state, userData: [...state.userData, action.payload] };
      } else {
        return { ...state, userData: state.userData };
      }

    case CONVERSATION_TYPE.CHAT_SCREEN_CLOSE:
      let closeData = state.userData;
      let index = closeData.findIndex(x => x.name === action.payload.name);
      state.userData.splice(index, 1);
      return { ...state, userData: state.userData };

    case CONVERSATION_TYPE.NEW_MESSAGE:

    let newConversationList = [...state.conversationList]
      let findVal = newConversationList.find(
        val => val.channel_id === action.payload.channel.sid
      );
      let newObj = action.payload.state;
      newObj.channelSid = action.payload.channel.sid;

      let newmsg = [...findVal.messages, newObj];
      let newAttributes = JSON.stringify(action.payload.state.attributes);
      action.payload.state.attributes = newAttributes;
      newConversationList.forEach(channel => {
        if (channel.channel_id === action.payload.channel.sid) {
          channel.messages.push(newmsg[newmsg.length - 1]);
          let a = newConversationList.indexOf(channel)
          let findVal2 = newConversationList[a]
          newConversationList.splice(a,1)
           newConversationList.splice(0, 0, findVal2)
        }
        return;
      });

      return { ...state, conversationList: newConversationList };

    default:
      return state;
  }
};
