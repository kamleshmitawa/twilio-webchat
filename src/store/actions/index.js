import {
  conversationList,
  chatScreenOpen,
  chatScreenClose,
  newMessage
} from "./conversationActions";

export default {
  conversationList: conversationList,
  chatScreenOpen: chatScreenOpen,
  chatScreenClose: chatScreenClose,
  newMessage: newMessage
};
